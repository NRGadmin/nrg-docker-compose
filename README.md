# docker-compose

Docker compose files used to deploy NRG admin services and stacks on a swarm or docker host.

## cfssl-interactive.yaml

This starts the [cfssl](https://hub.docker.com/r/cfssl/cfssl/) container used to generate TLS cert/key pairs to 
encrypt communication between Docker Swarm nodes.  The cert/key pairs are signed with a self-signed CA cert 
nrg-swarm-ca.pem, which as of this writing was generated 3/5/2019 and *expires 3/3/2024*.

### Volumes

This container needs 2 volumes.  TODO: relocate them to secure locations in ZFS with backup provision.

* cfssl_data:  Config and state information for cfssl
* swarm_certs: The CA nrg-swarm-ca.pem, TLS cert/key pairs generated from it, helper scripts

### Usage

To start the container defined in cfssl-interactive.yaml, docker-compose needs to be pointed to TLS cert/key pair so 
it can talk to the docker endpoint on port 2376:

```
# docker-compose --tlsverify --tlscacert=/etc/docker/tls/nrg-swarm-ca.pem --tlscert=/etc/docker/tls/swarm-mgr0.nrg.wustl.edu.pem --tlskey=/etc/docker/tls/swarm-mgr0.nrg.wustl.edu-key.pem -H swarm-mgr0.nrg.wustl.edu:2376 -f cfssl-interactive.yaml -p cfssl_interactive up -d --remove-orphans
```

Once the container is running, you can launch an interactive shell into it to get access to cfssl and cfssljson commmands:
```
# docker --tlsverify --tlscacert=/etc/docker/tls/nrg-swarm-ca.pem --tlscert=/etc/docker/tls/swarm-mgr0.nrg.wustl.edu.pem --tlskey=/etc/docker/tls/swarm-mgr0.nrg.wustl.edu-key.pem -H swarm-mgr0.nrg.wustl.edu:2376 exec -it cfssl_interactive_cfssl_interactive_nrg_1 /bin/bash
```

The swarm_certs volume has helper scripts like generate_certs_mired.sh illustrating cfssl and cfssljson command line 
usage to spin new cert/key pairs.  For example:

```
#!/bin/bash

CA="/swarm_certs/nrg-swarm-ca.pem"
CA_KEY="/swarm_certs/nrg-swarm-ca-key.pem"
CA_CONFIG="/swarm_certs/nrg-swarm-ca-config.json"

#edit as needed
PEERS="
mired-swarm-mgr0.nrg.wustl.edu
mired-swarm00.nrg.wustl.edu"

CLIENTS="
mired01.nrg.wustl.edu"

# generate certs for swarm nodes
for p in $PEERS ; do
  echo '{"CN":"$p","hosts":[""],"key":{"algo":"rsa","size":4096}}' | cfssl gencert -ca=$CA -ca-key=$CA_KEY -config=$CA_CONFIG -profile=peer -hostname="$p" - | cfssljson -bare $p
done

# certs for clients
for c in $CLIENTS ; do
  echo '{"CN":"$c","hosts":[""],"key":{"algo":"rsa","size":4096}}' | cfssl gencert -ca=$CA -ca-key=$CA_KEY -config=$CA_CONFIG -profile=client -hostname="$c" - | cfssljson -bare $c
done
```

Once generated, the cert/key pairs can then be scp'ed to the relevant machine to live under /etc/docker/tls (for swarm 
nodes) and /opt/xnat/tls (for XNATs needing to talk to a swarm manager).


## stack-monitoring.yaml

This starts up a [Portainer](https://www.portainer.io/) and [swarm-viz](https://github.com/mikesir87/swarm-viz) 
container on the Docker swarm node swarm-mgr0.nrg.wustl.edu.  Portainer expects a TLS cert/key pair to talk to the 
docker endpoint on port 2376.  Note these containers ended up not being adopted for routine use at NRG.  Leaving this 
compose YAML here for future reference.
